## GitLab Staging Snapshots

### How to enable snapshots for a drive

 Please see https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/gcp-snapshots.md

### Service account creation

In [`config-mgmt`](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt), add
a service account `gitlab-ci-snapshot-bot`, bound to a custom role
`gitlab.ci.snapshotter` with the following permissions in the targeted environment:

* `compute.disks.createSnapshot`
* `compute.disks.list`
* `compute.globalOperations.get`
* `compute.images.create`
* `compute.images.delete`
* `compute.images.get`
* `compute.images.list`
* `compute.images.setLabels`
* `compute.images.useReadOnly`
* `compute.resourcePolicies.list`
* `compute.snapshots.create`
* `compute.snapshots.delete`
* `compute.snapshots.get`
* `compute.snapshots.list`
* `compute.snapshots.setLabels`
* `compute.snapshots.useReadOnly`

Then add it to the impersonated service accounts in Vault (`vault-production` environment) for this project.

See for example:

* https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/91233362ee14c259b8e45c7a3e494eacd6025fd8/environments/gprd/iam.tf#L11-52
* https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/91233362ee14c259b8e45c7a3e494eacd6025fd8/environments/vault-production/gcp_projects.tf#L70

### Project creation

Add the new `gitlab-${env}-snapshots` project in [`infra-mgmt`](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/), see for example:

* https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/37f74bd97fce100e216886d846ddbe80d0d57c54/environments/ops-gitlab-net/projects_gitlab-restore.tf#L65-122
* https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/37f74bd97fce100e216886d846ddbe80d0d57c54/environments/gitlab-com/projects_gitlab-restore.tf#L31-59
