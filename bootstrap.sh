#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# bootstrap.sh file for restore-snapshot instnace
# passed as startup-script parameter from restore.sh
set -eufo pipefail
IFS=$'\t\n'

# just mount the drive for now
mount "/dev/disk/by-id/google-test-restore-disk-staging" /mnt
