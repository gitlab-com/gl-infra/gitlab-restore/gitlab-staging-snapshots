#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This script automates restoration of snapshot under gitlab-staging-1 project
set -eufo pipefail
IFS=$'\t\n'

output_date() {
	date "+%Y.%m.%d-%H:%M:%S %Z"
}

log() {
	echo "$(output_date) $*"
}

### Command requirements
command -v gcloud >/dev/null 2>/dev/null || {
	log 'Please install gcloud utility'
	exit 1
}

GCE_ZONE="${GCE_ZONE:-us-east1-c}"
GCE_PROJECT="${GCE_PROJECT:-gitlab-staging-1}"
GCE_RESTORE_PROJECT="${GCE_RESTORE_PROJECT:-gitlab-restore}"
INSTANCE_NAME="restore-snapshot-staging"
RESTORE_DISK_NAME="test-restore-disk-staging"
IMAGE_LABEL="created_by=gitlab-staging-snapshots"	# one label can be embedded as is

function cleanup() {
	# cleanup instance and disk in gitlab-restore project
	log "Cleaning up '${INSTANCE_NAME}' instance and '${RESTORE_DISK_NAME}' disk in project '${GCE_RESTORE_PROJECT}' ..."
	gcloud config set project "${GCE_RESTORE_PROJECT}"
	gcloud compute instances delete \
		"${INSTANCE_NAME}" \
		--delete-disks=all \
		--quiet || true

	# cleanup old images in gitlab-staging-1
	log "Cleaning up images in project '${GCE_PROJECT}' ..."
	gcloud config set project "${GCE_PROJECT}"
	gcloud compute images list \
		--no-standard-images \
		--filter="labels.${IMAGE_LABEL} AND status=READY" \
		--format="value(name)" | \
		xargs -r gcloud compute images delete --quiet || true
}

trap cleanup EXIT ERR
# Previous jobs terminated by the runner won't get signalled, so we do a cleanup this early to catch any leftovers.
cleanup

# switch to gitlab-staging-1 by default
gcloud config set project "${GCE_PROJECT}"
gcloud config set compute/zone "${GCE_ZONE}"

SNAPSHOT_FILTERS="storageBytes>0 AND status=('READY') AND creationTimestamp>'$(date -d '1 hour ago')'"
SNAPSHOT_NAME="${1:-randomize}"
if [[ "${SNAPSHOT_NAME}" == "randomize" ]]; then
	# shellcheck disable=SC2016
	log 'No snapshot specified as $1, will use random one: '
	SNAPSHOT_NAME="$(gcloud compute snapshots list \
		--filter="${SNAPSHOT_FILTERS}" \
		--format="value(name)" | \
		sort -R | head -1)"
	log "Random snapshot selected: '${SNAPSHOT_NAME}'"
elif [[ "${SNAPSHOT_NAME}" == "latest-patroni" ]]; then
	log 'Latest Patroni snapshot requested'
	SNAPSHOT_NAME="$(gcloud compute snapshots list \
		--filter="${SNAPSHOT_FILTERS} AND sourceDisk ~ patroni" \
		--sort-by='~creationTimestamp' \
		--format="value(name)" | \
		head -1 | awk '{print $1}')"
	log "Latest Patroni snapshot selected: '${SNAPSHOT_NAME}'"
fi

if [[ -z "${SNAPSHOT_NAME}" ]]; then
	log "Somehow, there was no non-empty and READY snapshots in the last hour. This is important and needs human attention!"
	exit 42
fi

if [[ "${CI_JOB_ID:-notonCI}" == "notonCI" ]]; then
	IMAGE_DESCRIPTION="Image from snapshot ${SNAPSHOT_NAME}, created manually by ${USER}@${HOSTNAME} on $(date)"
else
	IMAGE_DESCRIPTION="Image from snapshot ${SNAPSHOT_NAME}, created automatically by CI job '${CI_JOB_ID}' on $(date)"
fi

# TODO: better way to convert to ISO format (w/o second(s) fraction)
# shellcheck disable=SC1117
IMAGE_NAME="gitlab-restore-$(gcloud compute snapshots describe "${SNAPSHOT_NAME}" \
	--format="value[separator='-'](sourceDisk.basename(),creationTimestamp.date(tz_default='UTC',format='%s').sub('\.[0-9]{2,}?$', ''))")"

gcloud version

log "Creating image from snapshot '${SNAPSHOT_NAME}' ..."
gcloud compute images create "${IMAGE_NAME}" \
	--quiet \
	--source-snapshot="${SNAPSHOT_NAME}" \
	--description="${IMAGE_DESCRIPTION}" \
	--labels="${IMAGE_LABEL}" # this is in order not to give compute.globalOperations.get permission in gitlab-staging-1

# Switch to gilab-restore project
gcloud config set project "${GCE_RESTORE_PROJECT}"
gcloud config set compute/zone "${GCE_ZONE}"

# use 30 minutes timeout by default, try each minute
TIMEOUT=$((30*60))
SLEEP=60
# create disk from image in gitlab-restore
# TODO: wrap in function
# Specify --zone until https://issuetracker.google.com/issues/129362599 is resolved
until gcloud compute disks create \
		"${RESTORE_DISK_NAME}" \
		--type=pd-standard \
		--image-project="${GCE_PROJECT}" \
		--image="${IMAGE_NAME}" \
    --zone "${GCE_ZONE}" || [[ "${TIMEOUT}" -le 0 ]]; do
	TIMEOUT=$((TIMEOUT - SLEEP))
	log "Sleeping for ${SLEEP} seconds then will retry, deadline remaining: ${TIMEOUT} seconds ..."
	sleep "${SLEEP}"
done

# create instance with disk in gitlab-restore
gcloud compute instances create \
	"${INSTANCE_NAME}" \
	--preemptible \
	--boot-disk-size "30GB" \
	--boot-disk-type "pd-standard" \
	--disk "name=${RESTORE_DISK_NAME},device-name=${RESTORE_DISK_NAME}" \
	--machine-type "n1-standard-1" \
	--image-project "ubuntu-os-cloud" \
	--image-family "ubuntu-2004-lts" \
	--service-account "gitlab-restore-ci@gitlab-restore.iam.gserviceaccount.com" \
	--scopes "default,https://www.googleapis.com/auth/cloudkms" \
	--metadata-from-file startup-script=bootstrap.sh \
	--zone "${GCE_ZONE}"

# catch result
# TODO: wrap in function too
TIMEOUT=$((10*60))	# this is faster than above
SLEEP=30
until gcloud compute ssh \
		"${INSTANCE_NAME}" \
		--command='mount | grep /mnt' || [[ ${TIMEOUT} -le 0 ]]; do
	TIMEOUT=$((TIMEOUT - SLEEP))
	log "Sleeping for ${SLEEP} seconds then will retry, deadline remaining: ${TIMEOUT} seconds ..."
	sleep "${SLEEP}"
done

SUCCESS=1
if [[ ${TIMEOUT} -gt 0 ]]; then
	SUCCESS=0
	gcloud compute snapshots add-labels \
		"${SNAPSHOT_NAME}" \
		--labels="ci_restore=successs,ci_pipeline=${CI_JOB_ID}" \
		--project="${GCE_PROJECT}"
else
	gcloud compute snapshots add-labels \
		"${SNAPSHOT_NAME}" \
		--labels="ci_restore=failure,ci_pipeline=${CI_JOB_ID}" \
		--project="${GCE_PROJECT}"
fi

exit $SUCCESS	# pass the code back to CI
