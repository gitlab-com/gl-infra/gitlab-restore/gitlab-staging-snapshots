#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This script automates creation of snapshots under gitlab-staging-1 project
set -eufo pipefail
IFS=$'\t\n'

output_date() {
	date "+%Y.%m.%d-%H:%M:%S %Z"
}

log() {
	echo "$(output_date) $*"
}

### Command requirements
command -v gcloud >/dev/null 2>/dev/null || {
	log 'Please install gcloud utility'
	exit 1
}

GCE_ZONE=${GCE_ZONE:-us-east1-c}
GCE_PROJECT=${GCE_PROJECT:-gitlab-staging-1}

CI_JOB_ID=${CI_JOB_ID:-notonCI}
BATCH_SIZE=100

currentSnapshotPolicies=$(gcloud compute resource-policies list --project="$GCE_PROJECT" --filter='snapshotSchedulePolicy:*' --format='value(id)')

# snapshots before this date will be deleted
CLEANUP_DATE="$(date --date="14 days ago" +%Y-%m-%d)"

log "Querying manual snapshots to delete (older than ${CLEANUP_DATE}) ..."
declare -a TO_DELETE
# Filters used:
# * `creationTimestamp<'${CLEANUP_DATE}'`: Select snapshots older than the supplied date
# * `-sourceSnapshotSchedulePolicy:*`: Select snapshots that were not created by a snapshot policy (e.g. Patroni snapshots)
# * `NOT sourceSnapshotSchedulePolicyId=(${currentSnapshotPolicies})`: Select snapshots that were created by a snapshot policy that no longer exists and so a manual cleanup is required for them
readarray -t TO_DELETE < <(gcloud --project="$GCE_PROJECT" compute snapshots list \
	--filter="creationTimestamp<'${CLEANUP_DATE}' AND (-sourceSnapshotSchedulePolicy:* OR NOT sourceSnapshotSchedulePolicyId=(${currentSnapshotPolicies}))" \
	--uri)
log "${#TO_DELETE[@]} snapshots found."

if [[ ${#TO_DELETE[@]} -gt 0 ]]; then
	log "Cleaning up ${#TO_DELETE[@]} old snapshots ..."
	for ((i = 0; i < ${#TO_DELETE[@]}; i += BATCH_SIZE)); do
		gcloud --project="$GCE_PROJECT" compute snapshots delete "${TO_DELETE[@]:i:BATCH_SIZE}" \
			--quiet >/dev/null 2>/dev/null || true
	done
fi

log "Querying snapshots to delete (of size 0 bytes) ..."
declare -a EMPTY
readarray -t EMPTY < <(gcloud --project="$GCE_PROJECT" compute snapshots list \
	--filter="storageBytes=0 AND status=('READY')" \
	--uri)
log "${#EMPTY[@]} snapshots found."

if [[ "${#EMPTY[@]}" -gt 0 ]]; then
	log "Cleaning up ${#EMPTY[@]} empty snapshots ..."
	for ((i = 0; i < ${#EMPTY[@]}; i += BATCH_SIZE)); do
		gcloud --project="$GCE_PROJECT" compute snapshots delete "${EMPTY[@]:i:BATCH_SIZE}" \
			--quiet >/dev/null 2>/dev/null || true
	done
	log "Cleanup of empty snapshots completed"
fi
